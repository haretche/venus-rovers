package com.haretche.java.personal.venusrover.navigation;

/**
 * Represents the 4 directions that a machine can be facing (N,E,S and W)
 * @author Santiago Haretche
 *
 */
public enum Direction {
	NORTH('N'),
	EAST('E'),
	SOUTH('S'),
	WEST('W');
	
	public char asChar() {
        return asChar;
    }

    private final char asChar;

    private Direction(char asChar) {
        this.asChar = asChar;
    }
}
