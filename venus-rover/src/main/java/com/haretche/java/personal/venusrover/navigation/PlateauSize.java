package com.haretche.java.personal.venusrover.navigation;

/**
 * Contains the boundaries of a plateau.
 * @author Santiago Haretche
 *
 */
public class PlateauSize {
	private long north;
	private long east;
	private long south;
	private long west;
	
	public long getNorth() {
		return north;
	}
	public void setNorth(long north) {
		this.north = north;
	}
	public long getEast() {
		return east;
	}
	public void setEast(long east) {
		this.east = east;
	}
	public long getSouth() {
		return south;
	}
	public void setSouth(long south) {
		this.south = south;
	}
	public long getWest() {
		return west;
	}
	public void setWest(long west) {
		this.west = west;
	}
	
	@Override
	public String toString(){
		return(getNorth()+" "+getEast()+" "+getSouth()+ " " + getWest());
	}
}
