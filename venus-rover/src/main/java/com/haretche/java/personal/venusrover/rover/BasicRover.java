package com.haretche.java.personal.venusrover.rover;

import java.util.ArrayList;
import java.util.List;

import com.haretche.java.personal.venusrover.navigation.NavigationHelper;
import com.haretche.java.personal.venusrover.navigation.Position;

/**
 * Provides an implementation of a Rover with the basic capabilities (turning left, turning right and moving).
 * Don't modify this class to add new commands, extend this class or Rover instead.
 * @author Santiago Haretche
 *
 */
public class BasicRover extends Rover{
	
	private HardwareActuator hardwareActuator; 
	
	public BasicRover(Position startinPosition) {
		super(getCommandsKnownByBasicRover(),startinPosition);
		setHardwareActuator(new HardwareActuator());
	}
	
	public static List<Command> getCommandsKnownByBasicRover(){
		List<Command> commandsKnownByBasicRover= new ArrayList<Command>();
		commandsKnownByBasicRover.add(Command.MOVEFORWARD);
		commandsKnownByBasicRover.add(Command.TURNLEFT);
		commandsKnownByBasicRover.add(Command.TURNRIGHT);
		return commandsKnownByBasicRover;
	}
	
	private HardwareActuator getHardwareActuator() {
		return hardwareActuator;
	}

	private void setHardwareActuator(HardwareActuator hardwareActuator) {
		this.hardwareActuator = hardwareActuator;
	}

	@Override
	protected String executeBatchOfCommands(List<Command> commandsToExecute) {
		for(Command commandToExecute: commandsToExecute){
			switch(commandToExecute){
				case TURNLEFT:turnLeft();
					break;
				case MOVEFORWARD:moveOneStepForward();
					break;
				case TURNRIGHT:turnRight();
					break;
			}			
		}
		return this.getPosition().toString();
	}
	
	private void turnLeft(){
		this.getHardwareActuator().turnLeft();
		this.getPosition().setDirection(NavigationHelper.directionToTheLeftOf( this.getPosition().getDirection()));
	}
	
	private void turnRight(){
		this.getHardwareActuator().turnRight();
		this.getPosition().setDirection(NavigationHelper.directionToTheRightOf( this.getPosition().getDirection()));
	}
	
	private void moveOneStepForward(){
		if(NavigationHelper.plateauContainsPosition(RoversTeamManager.getInstance().getPlateauSize(), this.getPosition())){
			if(NavigationHelper.canMoveSafely(RoversTeamManager.getInstance().getPlateauSize(), this.getPosition())){
				//If rover is in the plateau and won't fall out of it by moving...
				this.getHardwareActuator().moveForward();
				NavigationHelper.updatePositionAfterMoving(this.getPosition());
			}
		}
	}
}
