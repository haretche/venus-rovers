package com.haretche.java.personal.venusrover.rover;

/** 
 * Knows how to interact with the hardware of the Rover 
 * @author Santiago Haretche
 *
 */
public class HardwareActuator {
	public void moveForward(){
		// TODO Implement functionality to interact with hardware 
	}
	public void turnLeft(){
		// TODO Implement functionality to interact with hardware 
	}
	public void turnRight(){
		// TODO Implement functionality to interact with hardware 
	}
}
