package com.haretche.java.personal.venusrover.rover;

/**
 * Set of all commands that rovers can execute.
 * The elements of this enum should be representable as char. 
 * @author Santiago Haretche
 *
 */
public enum Command {
	TURNLEFT('L'), TURNRIGHT('R'), MOVEFORWARD('M');
	
	public char asChar() {
        return asChar;
    }

    private final char asChar;

    private Command(char asChar) {
        this.asChar = asChar;
    }
}
