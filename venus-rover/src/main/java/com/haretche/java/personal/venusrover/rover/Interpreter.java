package com.haretche.java.personal.venusrover.rover;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.haretche.java.personal.venusrover.navigation.Direction;
import com.haretche.java.personal.venusrover.navigation.PlateauSize;
import com.haretche.java.personal.venusrover.navigation.Position;

/**
 * Knows the format expected in lines received from the console, and how to map those lines into objects.   
 * @author Santiago Haretche
 *
 */
public class Interpreter {
	
	/**
	 * Receives a line (String) with the size of a plateau and maps it into a PlateauSize object. 
	 * @param line Input in the format "Height North [space] Width East [space] Height South [space] Width West".
	 * @return A PlateauSize object with the size defined in the input line. 
	 * @throws BadInputException If the format of line is wrong.
	 */
	public static PlateauSize getPlateauSizeFromLine(String line) throws BadInputException{
		if(line==null){
			throw new BadInputException("Input line cannot be null."); 
		}
		PlateauSize plateauSize=new PlateauSize();
		String[] lineComponents = line.split(" ");
		if(lineComponents.length!=4){
			throw new BadInputException("4 numbers separated by spaces expected.");
		}
		try{
			plateauSize.setNorth(Long.parseLong(lineComponents[0]));
			plateauSize.setEast(Long.parseLong(lineComponents[1]));
			plateauSize.setSouth(Long.parseLong(lineComponents[2]));
			plateauSize.setWest(Long.parseLong(lineComponents[3]));
		}catch(NumberFormatException e){
			throw new BadInputException("The 4 elements have to be integer numbers.");
		}
		if(plateauSize.getNorth()<0 || plateauSize.getEast()<0 || plateauSize.getSouth()<0 || plateauSize.getWest()<0){
			throw new BadInputException("The 4 numbers cannot be negative.");
		}
		return plateauSize;
	}
	
	/**
	 * Receives a line (String) with a position and maps it into a Position object.
	 * @param line Input in the format "X-coordinate [space] Starting Y-coordinate [space] Direction the rover is facing (N, E, S or W)".
	 * @return A Position object with the coordinates and direction defined in the input line.
	 * @throws BadInputException If the format of line is wrong.
	 */
	public static Position getPositionFromLine(String line) throws BadInputException{
		if(line==null){
			throw new BadInputException("Input line cannot be null."); 
		}
		Position position=new Position();
		String[] lineComponents = line.split(" ");
		if(lineComponents.length!=3){
			throw new BadInputException("2 numbers and a direction were expected.");
		}
		try{			
			position.setX(Long.parseLong(lineComponents[0]));
			position.setY(Long.parseLong(lineComponents[1]));
		}catch(NumberFormatException e){
			throw new BadInputException("First 2 elements have to be integer numbers");
		}
		switch(lineComponents[2]){
			case "N":position.setDirection(Direction.NORTH);
				break;
			case "E":position.setDirection(Direction.EAST);
				break;
			case "S":position.setDirection(Direction.SOUTH);
				break;
			case "W":position.setDirection(Direction.WEST);
				break;
			default:
				throw new BadInputException("The third component has to be a direction: N,E,S or W");
		}		
		return position;
	}

	/**
	 * Receives a line (String) with a batch of commands to execute, validates that the rover on which they will be executed knows them,
	 * and maps them into a List of Command elements.
	 * @param commandsKnownByMachine List of commands known by the machine (in this case a rover) on which the commands will be executed. 
	 * @param line Input with a batch of commands to execute (each represented by a character of the line).
	 * @return The batch of commands to execute as a list of Command enum elements.
	 * @throws BadInputException If any of the characters of the line cannot be mapped to Command known by the machine.
	 */
	public static List<Command> getCommandsFromLine(List<Command> commandsKnownByMachine, String line) throws BadInputException{
		if(line==null){
			throw new BadInputException("Line is null"); 
		}
		List<Command> batchOfCommandsToExecute = new ArrayList<Command>();

		// Create searchable HashMap with the commandsKnownByMachine
		HashMap<Character, Command> mapOfCommandsKnownByMachine=new HashMap<Character, Command>();
		for(Command knownCommand:commandsKnownByMachine){
			mapOfCommandsKnownByMachine.put(knownCommand.asChar(), knownCommand);
		}
		
		char[] elementsOfLine = line.toCharArray();
		for(char elementOfLine: elementsOfLine){
			if(!mapOfCommandsKnownByMachine.containsKey(elementOfLine)){
				throw new BadInputException("Command "+elementOfLine+ " not recognized.");
			}else{
				batchOfCommandsToExecute.add(mapOfCommandsKnownByMachine.get(elementOfLine));
			}
		}
		return batchOfCommandsToExecute;
	}
}
