package com.haretche.java.personal.venusrover.rover;

/** 
 * Used within the rover when a line received from the console has a wrong format. Should be converted to a string before responding to the console. 
 * @author Santiago Haretche
 *
 */
public class BadInputException extends Exception{

	private static final long serialVersionUID = 1L;	
	
	public BadInputException(String message){
		super(message);
	}
}
