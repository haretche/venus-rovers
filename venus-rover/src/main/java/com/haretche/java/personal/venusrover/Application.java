package com.haretche.java.personal.venusrover;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.haretche.java.personal.venusrover.rover.RoversTeamManager;

public class Application {

public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome to Venus Rovers control application.");
		System.out.println("Please enter the lines to send to the Rovers...");
		try {
			while(true){
				String inputLine = reader.readLine();
		        String response = RoversTeamManager.getInstance().processLine(inputLine);
		        if(response!=null && !response.isEmpty()){
		        	//Something was received from the Rover
		        	if(!response.contains("Error")){
		        		System.out.println(response);
		        	}else{
		        		System.out.println(response +" Please enter line again.");	
		        	}		        	
		        }
			}
		} catch (IOException e) {
            e.printStackTrace();
        }
	}		
}
