package com.haretche.java.personal.venusrover.rover;

import java.util.List;

import com.haretche.java.personal.venusrover.navigation.Position;

/**
 * Defines the generic behavior of a rover without defining how commands must be executed. 
 * A basic implementation of a rover is also provided in this version. 
 * New implementations of a rover should be created when more commands are defined.    
 * @author Santiago Haretche
 *
 */
public abstract class Rover{
	
	private Position position;
	
	/**
	 * List with all the commands that a particular implementation of Rover knows how to execute.
	 */
	private List<Command> commandsKnownByRover;
	
	protected Rover(List<Command> commandsKnownByRover, Position startingPosition){
		this.setCommandsKnownByRover(commandsKnownByRover);
		this.setPosition(startingPosition);
	}

	public Position getPosition() {
		return position;
	}

	private void setPosition(Position position) {
		this.position = position;
	}

	public List<Command> getCommandsKnownByRover() {
		return commandsKnownByRover;
	}
	
	private void setCommandsKnownByRover(List<Command> commandsKnownByRover) {
		this.commandsKnownByRover = commandsKnownByRover;
	}

	/**
	 * Asks the rover to execute a batch of commands. If a command is not known by the rover it will be skipped.  
	 * @param commandsToExecute Batch of commands to execute.
	 * @return Final position of the Rover after executing the batch of commands.
	 */
	protected abstract String executeBatchOfCommands (List<Command> commandsToExecute) ;
}
