package com.haretche.java.personal.venusrover.navigation;

public class Position {
	private long x;
	private long y;
	private Direction direction;
	
	public long getX() {
		return x;
	}
	public void setX(long x) {
		this.x = x;
	}
	public long getY() {
		return y;
	}
	public void setY(long y) {
		this.y = y;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	@Override
	public String toString(){
		return(getX()+" "+getY()+" "+getDirection().asChar());
	}
}
