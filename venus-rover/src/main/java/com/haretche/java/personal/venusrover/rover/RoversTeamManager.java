package com.haretche.java.personal.venusrover.rover;

import java.util.List;

import com.haretche.java.personal.venusrover.navigation.NavigationHelper;
import com.haretche.java.personal.venusrover.navigation.PlateauSize;
import com.haretche.java.personal.venusrover.navigation.Position;

/**
 * This class represents the manager of the rovers. It processes the lines received from the console and decides what Rover to forward the commands to.
 * In initial version of this class, I assumed that I can send commands to rovers in any position of the plateau.
 * Singleton pattern is used. 
 * @author Santiago Haretche
 *
 */
public class RoversTeamManager {
	private PlateauSize plateauSize;
	private Position positionOfRoverWaitingCommand;//The following line received from the console will be sent to the rover in this position. 
	protected static RoversTeamManager instance;
	
	public synchronized static RoversTeamManager getInstance(){
		if(instance==null){
			instance = new RoversTeamManager();
		}
		return instance;
	}
	
	protected RoversTeamManager(){
		// Restricts other classes from instantiating RoversTeamManager (Singleton Pattern).
	}	
	
	public PlateauSize getPlateauSize() {
		return plateauSize;
	}

	private Position getPositionOfRoverWaitingCommand() {
		return positionOfRoverWaitingCommand;
	}

	private void setPositionOfRoverWaitingCommand(Position positionOfRoverWaitingCommand) {
		this.positionOfRoverWaitingCommand = positionOfRoverWaitingCommand;
	}

	private void setPlateauSize(PlateauSize plateauSize) {
		this.plateauSize = plateauSize;
	}

	/**
	 * Sends a line with instructions for the Rovers Team  
	 * @param line Line to be processed by the Rovers Team
	 * @return If an error occurred, the error message starting with 'Error'. If a batch of commands was sent, returns the updated position of the rover. 
	 * Returns null otherwise. I decided to return a String instead of a more elaborated object to be consistent with the message sent to the rovers.
	 * */
	public synchronized String processLine(String line) {  
		if(getPlateauSize()==null){
			//The first line received should contain the size of the plateau. 
			try{
				setPlateauSize(Interpreter.getPlateauSizeFromLine(line));
				return null;
			}catch(BadInputException e){
				return "Error: Couldn't convert line into plateau size: " + e.getMessage();
			}
		}else{
			if(getPositionOfRoverWaitingCommand()==null){
				// Line should contain the position of the rover. 
				try{
					Position positionReceivedFromLine=Interpreter.getPositionFromLine(line);
					if(NavigationHelper.plateauContainsPosition(getPlateauSize(), positionReceivedFromLine)){
						setPositionOfRoverWaitingCommand(Interpreter.getPositionFromLine(line));
					}else{
						return "Error: Rover must be located in a position contained in plateau of size: " + getPlateauSize().toString();
					}
					return null;
				}catch(BadInputException e){
					return "Error: Couldn't convert line into position: " + e.getMessage();
				}
			}else{
				// If there is a predefined list of rovers to which commands can be sent, the following line should be changed to reflect that.
				// Creates a new Rover and passes the position received in the prior line to it.
				Rover rover = new BasicRover(getPositionOfRoverWaitingCommand());
				try{
					List<Command> batchOfCommandsToExecute=Interpreter.getCommandsFromLine(rover.getCommandsKnownByRover(), line);
					String finalPositionOfRover= rover.executeBatchOfCommands(batchOfCommandsToExecute);
					setPositionOfRoverWaitingCommand(null);
					return finalPositionOfRover;
				}catch(BadInputException e){
					return "Error: Couldn't convert line into commands: " + e.getMessage();
				}
			}		
		}
	}

}
