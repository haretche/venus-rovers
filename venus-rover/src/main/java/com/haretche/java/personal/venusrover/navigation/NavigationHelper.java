package com.haretche.java.personal.venusrover.navigation;

/**
 * This class provides tools for turning and moving.
 * Could be used by other types of machines besides rovers.
 * @author Santiago Haretche
 *
 */
public class NavigationHelper {
	public static Direction directionToTheLeftOf(Direction initialDirection){
		switch(initialDirection){
			case NORTH: return Direction.WEST;
			case EAST: return Direction.NORTH;
			case SOUTH: return Direction.EAST;
			case WEST: return Direction.SOUTH;
			default: throw new IllegalStateException(); //Necessary to compile
		}
	}
	
	public static Direction directionToTheRightOf(Direction initialDirection){
		switch(initialDirection){
			case NORTH: return Direction.EAST;
			case EAST: return Direction.SOUTH;
			case SOUTH: return Direction.WEST;
			case WEST: return Direction.NORTH;
			default: throw new IllegalStateException(); //Necessary to compile
		}
	}
	
	/**
	 * Uses the position and direction of a rover and the size of the plateau to calculate whether the robot can move forward. 
	 * @param plateauSize The size of the plateau the rovers are deployed to.
	 * @param startingPosition Position of the rover before moving.
	 * @return True if moving would keep the rover within the plateau. False if moving would lead to the rover falling out of the plateau.
	 */
	public static boolean canMoveSafely(PlateauSize plateauSize, Position startingPosition){
		//TODO: I assumed that rovers have collision prevention sensors. If that is not the case, software to avoid ground irregularities and collisions must be added here.
		switch(startingPosition.getDirection()){
			case EAST: return startingPosition.getX()<plateauSize.getEast();
			case NORTH: return startingPosition.getY()<plateauSize.getNorth();
			case SOUTH: return startingPosition.getY()>(-plateauSize.getSouth());
			case WEST: return startingPosition.getX()>(-plateauSize.getWest());
			default: throw new IllegalStateException();		
		}
	}
	
	/**
	 * Updates the position assuming that the rover moved towards the direction it is facing.
	 * @param startingPosition Starting position of the rover, containing its direction.
	 */
	public static void updatePositionAfterMoving(Position startingPosition){
		switch(startingPosition.getDirection()){
			case EAST:  startingPosition.setX(startingPosition.getX()+1);
				break;
			case NORTH:  startingPosition.setY(startingPosition.getY()+1);
				break;
			case WEST:  startingPosition.setX(startingPosition.getX()-1);
				break;
			case SOUTH:  startingPosition.setY(startingPosition.getY()-1);
				break;
			default: throw new IllegalStateException();		
		}
	}
	
	public static boolean plateauContainsPosition(PlateauSize plateauSize, Position position){
		return position.getX()<=plateauSize.getEast() && position.getY()<=plateauSize.getNorth() 
				&& position.getX()>=(-plateauSize.getWest()) && position.getY()>=(-plateauSize.getSouth());
	}
}

