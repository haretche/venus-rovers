package com.haretche.java.personal.venusrover;

import com.haretche.java.personal.venusrover.rover.RoversTeamManagerTestMock;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ApplicationTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ApplicationTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ApplicationTest.class );
    }
    
    private String sendLineLogToConsole(String line){
    	System.out.println("Input: "+line);
    	String result=  RoversTeamManagerTestMock.getInstance().processLine(line);
    	if(result!=null){
    		System.out.println("Output: "+result);
    	}
    	return result;
    }
    
    private void assertContainsError(String output){
    	assertTrue(output!=null && output.contains("Error"));
    }
    
    /**
     * Deletes all information from previous tests and writes the name of the test in the console.
     * @param testName Name of the test that will start.
     */
    private void cleanEnvironment(String testName){
    	RoversTeamManagerTestMock.DeleteInstance();
    	System.out.println("");
    	System.out.println(testName+ " started execution...");
    }
    
    /**
     * Test case provided by the example.
     */
    public void testCaseFromExample()
    {    	
    	cleanEnvironment("testCaseFromExample");
    	
    	assertNull(sendLineLogToConsole("5 5 3 3"));
    	assertNull(sendLineLogToConsole("1 2 N"));
    	assertEquals("1 3 N",sendLineLogToConsole("LMLMLMLMM"));
    	assertNull(sendLineLogToConsole("-3 -1 E"));
    	assertEquals("2 -3 S",sendLineLogToConsole("MMRMMLMMMRM"));
    }
    
    /**
     * Test that the rovers don't fall out of the plateau even when instructed to do so.
     */
    public void testRoverShouldntFallOutOfPlateau()
    {   
    	cleanEnvironment("testRoverShouldntFallOutOfPlateau");
    	
    	assertNull(sendLineLogToConsole("3 4 5 6"));
    	assertNull(sendLineLogToConsole("1 2 N"));
    	assertEquals("1 3 N",sendLineLogToConsole("MM"));
    	assertNull(sendLineLogToConsole("1 3 N"));
    	assertEquals("4 3 E",sendLineLogToConsole("RMMMM"));
    	assertNull(sendLineLogToConsole("4 3 E"));
    	assertEquals("4 -5 S",sendLineLogToConsole("RMMMMMMMMM"));
    	assertNull(sendLineLogToConsole("4 -5 S"));
    	assertEquals("-6 -5 W",sendLineLogToConsole("RMMMMMMMMMMM"));
    }
    
    /**
     * Test that user cannot enter a rover starting position out of plateau.
     */
    public void testRoverCannotStartOutOfPlateau()
    {   
    	cleanEnvironment("testRoverCannotStartOutOfPlateau");    	
    	assertNull(sendLineLogToConsole("3 4 5 6"));
    	assertContainsError(sendLineLogToConsole("0 4 N"));
    	assertContainsError(sendLineLogToConsole("5 0 E"));
    	assertContainsError(sendLineLogToConsole("0 -6 S"));
    	assertContainsError(sendLineLogToConsole("-7 0 W"));
    }
}
